#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#define MAXLEN 256
#define PORT "9034"

#include "datatypes.h"	//struct für die user
#include "readxml.h"
#include "readusers.h"	//einlesefunktion
#include "socketconf.h" //socket bauen





int main(void){

//alles für sockets undso

	int ucnt=0, i, nbytes;
	int listener, fdmax, newfd;
	fd_set master;    // master file descriptor list
    fd_set read_fds;  // temp file descriptor list for select()
    struct sockaddr_storage remoteaddr; // client address
    //char  cmd[256],
    char msg[256], buf[256];
    //char cmdnotfound[]="ungültiger befehl\n";
    char remoteIP[INET6_ADDRSTRLEN];
    socklen_t addrlen;
//
    struct message *input;
	input=(struct message*)malloc(sizeof(struct message));

//alles für user einlesen
    //int zaehl;
    struct userdaten *online_users=NULL, *offline_users=NULL;
    offline_users=(struct userdaten*)malloc(sizeof(struct userdaten));
	ucnt=readusers("users.txt", offline_users);
	if(ucnt){
		printf("%i userdaten eingelesen!\n", ucnt);}else{
			printf("keine userdatendatei oder so?\n");}
//
	listener=buildlistener(&master);//socket bauen

	FD_ZERO(&read_fds);
	// keep track of the biggest file descriptor
    fdmax = listener; // so far, it's this one
	// main loop
    printf("socket gebaut, horchen....\n");
    for(;;) {
        read_fds = master; // copy it
        if (select(fdmax+1, &read_fds, NULL, NULL, NULL) == -1) {
            perror("select");
            exit(4);
        }
        // run through the existing connections looking for data to read
        for(i = 0; i <= fdmax; i++) {
            if (FD_ISSET(i, &read_fds)) { // we got one!!
                if (i == listener) {
                    // handle new connections
                    addrlen = sizeof remoteaddr;
					newfd = accept(listener,
						(struct sockaddr *)&remoteaddr,
						&addrlen);

					if (newfd == -1) {
                        perror("accept");
                    } else {
                        FD_SET(newfd, &master); // add to master set
                        if (newfd > fdmax) {    // keep track of the max
                            fdmax = newfd;
                        }
                        printf("selectserver: new connection from %s on "
                            "socket %d\n",
							inet_ntop(remoteaddr.ss_family,
								get_in_addr((struct sockaddr*)&remoteaddr),
								remoteIP, INET6_ADDRSTRLEN),
							newfd);
                    }
                } else {
                    // handle data from a client
                    if ((nbytes = recv(i, buf, sizeof buf , 0)) <= 0) {
                        // got error or connection closed by client
                        if (nbytes == 0) {
                            // connection closed
                            printf("selectserver: socket %d hung up\n", i);
                            empty(input);
                            setoffline(i, &offline_users, &online_users);

                        } else {
                            perror("recv");
                            //printf("nbytes was: %i", nbytes);
                        }
                        close(i); // bye!
                        FD_CLR(i, &master); // remove from master set
                    } else {
                        // we got some data from a client
                        if(readxml(buf, input)){	//input zerfriemeln und gucken...
                        	//für logins
                        	if(strcmp(input->type, "login")==0){
                        		if(searchattr(input, "name")&&searchattr(input, "hash")){
                        			if(setonline(searchattr(input, "name"), searchattr(input, "hash"), i, &offline_users, &online_users)){
                        				strcpy(msg, "mmmhokay. eingeloggt.\n");
                        				if (send(i, msg, strlen(msg) , 0) == -1) {
                        					perror("send");
                        				}
                        				printf("login: %s\n", searchattr(input, "name"));
                        			}else{
                        				strcpy(msg, "falsche daten, nicht eingeloggt.\n");
                        				if (send(i, msg, strlen(msg) , 0) == -1) {
                        					perror("send");
                        				}
                        				printf("login-try: %s\n", searchattr(input, "name"));
                        				printf("login-try: %s\n", searchattr(input, "hash"));
                        			}
                        		}else{
                        			strcpy(msg, "username und pw bitte...\n");
                        			if (send(i, msg, strlen(msg) , 0) == -1) {
                        				perror("send");
                        			}
                        		}
                        	}
                        	//-------ende login

                        	if(strcmp(input->type, "say")==0){
                        		//strcpy(msg, searchattr(input, "to") );
                        		//if(msg>0){
                        			sprintf(msg, "<say from='%s'>%s</say>", searchusername(i, &online_users), searchattr(input, "say"));
                        		//}
                        		if(searchsocket(searchattr(input, "to"), &online_users)!=0){
                        			if (send(searchsocket(searchattr(input, "to"), &online_users), msg, strlen(msg), 0) == -1){
                        				perror("send");
                        			}
                        		}else{
                        			strcpy(msg, "user nicht vorhanden oder nicht online.\n");
                        			if (send(i, msg, strlen(msg) , 0) == -1) {
                        				perror("send");
                        			}
                        		}
                        	}

                        	if(strcmp(input->type, "getlist")==0){}


                        	empty(input);
                        }/*else{		//wenn nicht mit / beginnt, raus an alle!
                        	for(j = 0; j <= fdmax; j++) {
                            	// send to everyone!
                            	if (FD_ISSET(j, &master)) {
                            		// except the listener and ourselves
                                	if (j != listener && j != i) {
                                    	if (send(j, buf, nbytes, 0) == -1) {
                                        	perror("send");
                                    	}
                                	}
                            	}
                        	}
                        }*/

                    }
                } // END handle data from client
            } // END got new incoming connection
        } // END looping through file descriptors
    } // END for(;;)--and you thought it would never end!


return 0;
}

