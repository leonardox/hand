/*
#include <string.h>
#include <stdio.h>
#include "datatypes.h"
#include <malloc.h>
*/

char* searchattr(struct message *msg, char *searchterm){
	//printf("suche nach %s...\n", searchterm);
	for(;strcmp(msg->type,searchterm)!=0 && (msg->next!=NULL) ; msg=msg->next){
		//printf("%s != %s\n", msg->type, searchterm);
	}
	if(strcmp(msg->type,searchterm)==0){
		return msg->content;
	}else{
		//printf("...nüx gefunden\n");
		return 0;
	}
}

void empty(struct message *msg){
	struct message *att;
	struct message *tmp;
	while(msg->next!=NULL){
		att=msg;
		while(att->next!=NULL){
			tmp=att;
			att=att->next;
		}
		tmp->next=NULL;
		free(att);
	}
	msg->content[0]='\0';
	msg->type[0]='\0';
	msg->full=0;
	msg->next=NULL;
}

void listeverlaengern(struct message *att){
	struct message *att_new;
	att_new=(struct message*)malloc(sizeof(struct message));
	att_new->content[0]='\0';
	att_new->type[0]='\0';
	att_new->next=NULL;
	att_new->full=0;
	att->next=att_new;
}

struct message* appendattrib(struct message *msg){
	struct message *att;
	att=msg;
	if(att->next!=NULL){
		att=att->next;
		while(att->next!=NULL){
			att=att->next;
		}
		if(att->full==false){
			return att;
		}else{
			listeverlaengern(att);
		}
	}else{
		listeverlaengern(att);
	}

	return att->next;
	}


bool readxml(char *buf, struct message *msg){
	int zaehl=0, ind=0;
	char *msgend, temp[100];
	lesezustand zustand=start;


while(zustand!=ende){
switch(zustand){

	case start:
		for(;(buf[zaehl]!='<');zaehl++){	//bis es losgeht - eigentlich nicht nötig
		if((buf[zaehl]=='\0') || zaehl>=512){//fehler - zu lang oder zuende ohne zuende zu sein
			printf("fehler beim typanfang finden!!! -> 0 zurück...\n");
			return 0;
			}
		}
		zaehl++;
		zustand=typlesen;
		break;
	case typlesen:
		for(;(buf[zaehl]!='>' && buf[zaehl]!=' ' && buf[zaehl]!='/');zaehl++){
			if((buf[zaehl]=='\0') || zaehl>=512){//fehler - zu lang oder zuende ohne zuende zu sein
				return 0;
				}
			msg->type[ind]=buf[zaehl];
			ind++;
		}
		if(buf[zaehl]=='>'){
			zustand=inhaltlesen;
			}
		if(buf[zaehl]==' '){
			zustand=leer;
			}
		if(buf[zaehl]=='/'){
			zustand=typendelesen;
			}
		msg->type[ind]='\0';
		zaehl++;
		break;

	case inhaltlesen:
		ind=0;
		strcpy(temp,"</\0");
		strcat(temp,msg->type);
		msgend=strstr(buf, temp);
		for(;(zaehl!=msgend - buf);zaehl++){
			if(zaehl>=512){//fehler - zu lang oder zuende ohne zuende zu sein
				return 0;
			}
			msg->content[ind]=buf[zaehl];
			ind++;
		}
		msg->content[ind]='\0';
		zustand=ende;
		zaehl++;
		break;

	case leer:
		for(;(buf[zaehl]==' ');zaehl++){}
		//zaehl++;
		if(buf[zaehl]=='>'){
			zustand=inhaltlesen;
			}else{
				if(buf[zaehl]=='/'){
					zustand=typendelesen;
				}else{
					zustand=fulltypelesen;}
			}
		break;

	case fulltypelesen:

		ind=0;
		for(;(buf[zaehl]!='=');zaehl++){
				appendattrib(msg)->type[ind]=buf[zaehl];//hier attribut lesen
				ind++;
			}
		appendattrib(msg)->type[ind]='\0';
		zaehl++;//=überspringen
		zaehl++;//'überspringen
		ind=0;
		for(;(buf[zaehl]!='\'');zaehl++){
				appendattrib(msg)->content[ind]=buf[zaehl];//hier attributinhalt lesen
				ind++;
			}
		appendattrib(msg)->content[ind]='\0';
		appendattrib(msg)->full=true;
		zaehl++; //letztes ' überspringen
		if(buf[zaehl]=='>'){
			zustand=inhaltlesen;
			}
		if(buf[zaehl]==' '){
			zustand=leer;
			}
		if(buf[zaehl]=='/'){
			zustand=typendelesen;
			}
		zaehl++;
		break;

	case typendelesen:
		zustand=ende;
	break;

	default:
		printf("fehler!!");
		return 0;
	break;

	}
}
return 1;

}
/*
int main(){
	char *msg="<xml to='usernr1' attr2='blinkenundfettschreiben'>testnachticht!!</xml>";
	struct message *cut;
	cut=(struct message*)malloc(sizeof(struct message));
	cut->attribute=NULL;
	cut->full=0;
	if(readxml(msg, cut)){
		printf("input: %s\n\n", msg);
		printf("type: %s\n", cut->type);
		printf("cont: %s\n\n", cut->content);
		while(cut->attribute!=NULL){
			printf("attrib: %s\n", cut->attribute->type);
			printf("attrib inhalt: %s\n\n", cut->attribute->content);
			cut=cut->attribute;
		}

		}
	return 0;
	}
*/
