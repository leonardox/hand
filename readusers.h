#define MAXLEN 256

int searchsocket(char *username, struct userdaten **online_users){
	struct userdaten *users_tmp;
	users_tmp=*online_users;
	for(;users_tmp!=NULL;users_tmp=users_tmp->next){
		if(strcmp(username, users_tmp->user)==0){
			return users_tmp->socket;
		}
	}
	return 0;
}

char* searchusername(int socket, struct userdaten **online_users){
	struct userdaten *users_tmp;
	users_tmp=*online_users;
	for(;users_tmp!=NULL;users_tmp=users_tmp->next){
		if(socket== users_tmp->socket){
			return users_tmp->user;
		}
	}
	return 0;
}


void printusers(struct userdaten **online_users){
	if(online_users!=NULL){
	struct userdaten *on_tmp=*online_users;
	printf("users:\n");

		printf("%s\n", on_tmp->user);
		for(;on_tmp->next!=NULL;on_tmp=on_tmp->next){
			printf("%s\n", on_tmp->next->user);
		}
	}
}

bool setoffline(int socket, struct userdaten **users, struct userdaten **online_users){
	struct userdaten *on_tmp, *off_tmp, *tmp;
		tmp=NULL;
		on_tmp=*online_users;
		off_tmp=*users;
		for(;on_tmp!=NULL;on_tmp=on_tmp->next){
			if(on_tmp->socket==socket){
				if(off_tmp!=NULL){	//nur wenn on_tmp nicht leer ist
					for(;off_tmp->next!=NULL;off_tmp=off_tmp->next){}	//on_tmp ist der erste zeiger ins nichts
					off_tmp->next=on_tmp;
					off_tmp=off_tmp->next;
					//off_tmp->next=NULL;

				}else{
						off_tmp=on_tmp;
						*users=off_tmp;
				}
				on_tmp=on_tmp->next;		//wenn das erste, den zeiger auf off_tmp eins weiter
				if(tmp!=NULL){
					tmp->next=on_tmp;			//sonst das vorige next auf das nächste element
				}
				off_tmp->next=NULL;
				if(on_tmp==NULL){
					online_users=NULL;
				}
				printusers(online_users);
				return 1;
			}
			tmp=on_tmp;
			//tmp=tmp->next;
		}
		//user nicht gefunden
	return 0;
}

bool setonline(char *user, char *pw, int socket, struct userdaten **users, struct userdaten **online_users){
	struct userdaten *on_tmp, *off_tmp, *tmp;
	tmp=NULL;
	on_tmp=*online_users;
	off_tmp=*users;

	for(;off_tmp!=NULL;off_tmp=off_tmp->next){
		if(strcmp(off_tmp->user, user)==0){	//usernamen gefunden
			if(strcmp(off_tmp->hash, pw)==0){	//pw passt
				off_tmp->socket=socket;			//socket setzen

				if(on_tmp!=NULL){	//nur wenn on_tmp nicht leer ist
					for(;on_tmp->next!=NULL;on_tmp=on_tmp->next){}	//on_tmp ist der erste zeiger ins nichts
					on_tmp->next=off_tmp;
					on_tmp=on_tmp->next;
					on_tmp->next=NULL;

				}else{
					on_tmp=off_tmp;
					*online_users=on_tmp;
				}
				off_tmp=off_tmp->next;		//wenn das erste, den zeiger auf off_tmp eins weiter
				if(tmp!=NULL){
					tmp->next=off_tmp;			//sonst das vorige next auf das nächste element
				}
				on_tmp->next=NULL;

				printusers(online_users);
				return 1;
			}
		}
		tmp=off_tmp;
		//tmp=tmp->next;
	}
	//user nicht gefunden
	return 0;
}




int readusers(char *path, struct userdaten *users){
   char buf[MAXLEN] = { 0 };
   int count=0;
   FILE * fp = fopen( path, "r" );

   if(!fp){
      // file not exists or not reachable
      return 0;
   }

	struct message *cut;
	cut=(struct message*)malloc(sizeof(struct message));

	struct userdaten *att, *att_new;
	att=users;

   //zeilenweise einlesen while not eof....
	while(fgets(buf, MAXLEN, fp) != NULL){
		if(readxml(buf, cut)){
			//if(strcmp(cut->next->type, "user")==0){
				strcpy(att->user,cut->next->content);//user
				strcpy(att->hash,cut->next->next->content);//pw
				printf("read: %s, %s\n", att->user, att->hash);
				empty(cut);


				att_new=(struct userdaten*)malloc(sizeof(struct userdaten));
				att->next=att_new;
				att=att->next;
				count++;
			//}
		}else{
			printf("defekte userdatei");
		}

	}
	free(cut);
				//strcpy(users[count].user,uname);
				//users[count].socket=-1;
				//users[count].online=0;
		//
   fclose(fp);
   return count;
}

